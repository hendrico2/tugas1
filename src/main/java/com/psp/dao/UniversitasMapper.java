package com.psp.dao;

import com.psp.models.ProdiModel;
import com.psp.models.UniversitasModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Get-Up on 4/8/2017.
 */
@Mapper
public interface UniversitasMapper {
    @Select("SELECT kode_univ, nama_univ, url_univ FROM univ ")
    @Results(
            value = {
                    @Result(property = "kode", column = "kode_univ"),
                    @Result(property = "nama", column = "nama_univ"),
                    @Result(property = "url", column = "url_univ"),
                    @Result(property = "listProdi", column = "kode_univ",
                            javaType = List.class,
                            many = @Many(select = "getAllProdiByKodeUniversitas")
                    )
            }
    )
    List<UniversitasModel> getAllUniversitas();

    @Select("SELECT " +
            "prodi.kode_univ, kode_prodi, nama_prodi " +
            "FROM univ JOIN prodi ON prodi.kode_univ = univ.kode_univ " +
            "WHERE prodi.kode_univ = #{kode_univ}")
    @Results(
            value = {
                    @Result(property = "kodeUniversitas", column = "kode_univ"),
                    @Result(property = "kodeProdi", column = "kode_prodi"),
                    @Result(property = "nama", column = "nama_prodi")
            }
    )
    List<ProdiModel> getAllProdiByKodeUniversitas(@Param("kode_univ") String kode_univ);

    @Select("SELECT kode_univ, nama_univ, url_univ FROM univ WHERE kode_univ = #{kode}")
    @Results(
            value = {
                    @Result(property = "kode", column = "kode_univ"),
                    @Result(property = "nama", column = "nama_univ"),
                    @Result(property = "url", column = "url_univ"),
                    @Result(property = "listProdi", column = "kode_univ",
                            javaType = List.class,
                            many = @Many(select = "getAllProdiByKodeUniversitas")
                    )
            }
    )
    UniversitasModel getUniversitasByKode(@Param("kode") String kodeUniversitas);
}
