package com.psp.dao;

import com.psp.models.PesertaModel;
import com.psp.models.ProdiModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Get-Up on 4/11/2017.
 */

@Mapper
public interface ProdiMapper {

    @Select("SELECT kode_univ, kode_prodi, nama_prodi FROM prodi WHERE kode_prodi = #{kode}")
    @Results(
            value = {
            @Result(property = "kodeProdi", column = "kode_prodi"),
            @Result(property = "kodeUniversitas", column = "kode_univ"),
            @Result(property = "nama", column = "nama_prodi"),
            @Result(
                    property = "listPeserta", column = "kode_prodi",
                    javaType = List.class, many = @Many(select = "getAllPesertaByKodeProdi"))
    })
    ProdiModel getProdiByKodeProdi(@Param("kode") String kodeProdi);

    @Select("SELECT nomor,nama,DATE_FORMAT(tgl_lahir,'%Y-%m-%d') as tgl,prodi.kode_prodi " +
            "FROM peserta JOIN prodi ON peserta.kode_prodi = prodi.kode_prodi " +
            "WHERE prodi.kode_prodi = #{kode}")
    @Results(
            value = {
            @Result(property = "nomor", column = "nomor"),
            @Result(property = "nama", column = "nama"),
            @Result(property = "tglLahir", column = "tgl"),
            @Result(property = "kodeProdi", column = "kode_prodi")
    })
    List<PesertaModel> getAllPesertaByKodeProdi(@Param("kode") String kode);
}