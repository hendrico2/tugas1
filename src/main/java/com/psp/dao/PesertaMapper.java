package com.psp.dao;

import com.psp.models.PesertaModel;
import org.apache.ibatis.annotations.*;


/**
 * Created by Get-Up on 4/7/2017.
 */

@Mapper
public interface PesertaMapper {
    @Select("SELECT nomor, nama, DATE_FORMAT(tgl_lahir,'%Y-%m-%d') as tgl, kode_prodi FROM peserta WHERE nomor = #{nomor}")
    @Results(
            value = {
                    @Result(property = "nomor", column = "nomor"),
                    @Result(property = "nama", column = "nama"),
                    @Result(property = "tglLahir", column = "tgl"),
                    @Result(property = "kodeProdi", column = "kode_prodi")
            }
    )
    PesertaModel getPesertaByNomor(@Param(value = "nomor") String nomor);
}
