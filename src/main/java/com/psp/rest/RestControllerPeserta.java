package com.psp.rest;

import com.psp.models.PesertaModel;
import com.psp.services.PesertaServiceImplentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by CO2 on 4/22/2017.
 */
@RestController
public class RestControllerPeserta {
    @Autowired
    PesertaServiceImplentation pesertaDAO;

    @GetMapping("/rest/peserta")
    public PesertaModel peserta(Model model, @RequestParam(value =
            "nomor", required = false) String nomor) {
        PesertaModel dataPeserta = pesertaDAO.getPeserta(nomor);
        if (dataPeserta == null)
            return null;
        else
            return dataPeserta;

    }
}
