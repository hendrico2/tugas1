package com.psp.rest;

import com.psp.models.ProdiModel;
import com.psp.models.UniversitasModel;
import com.psp.services.ProdiService;
import com.psp.services.UniversitasServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by CO2 on 4/22/2017.
 */
@RestController
public class RestControllerProdi {
    @Autowired
    ProdiService prodiDAO;


    @GetMapping("/prodi/{kode}")
    public ProdiModel prodi(Model model, @PathVariable("kode") String kodeProdi) {
        ProdiModel prodiModel = prodiDAO.getProdiByKodeProdi(kodeProdi);
        if (prodiModel == null) {
            return null;
        }
        return prodiModel;
    }
}
