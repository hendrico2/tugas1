package com.psp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Get-Up on 4/7/2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniversitasModel {
    private String kode;
    private String nama;
    private String url;
    private List<ProdiModel> listProdi;
}
