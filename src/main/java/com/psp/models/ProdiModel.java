package com.psp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Get-Up on 4/7/2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiModel {
    private String kodeUniversitas;
    private String kodeProdi;
    private String nama;
    private List<PesertaModel> listPeserta;
}
