package com.psp.services;

import com.psp.dao.ProdiMapper;
import com.psp.models.PesertaModel;
import com.psp.models.ProdiModel;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by Get-Up on 4/11/2017.
 */
@Log
@Service
public class ProdiServiceImplementation implements ProdiService {

    @Autowired
    private ProdiMapper prodiMapper;
    @Autowired
    private PesertaServices pesertaServices;

    @Override
    public ProdiModel getProdiByKodeProdi(String kodeProdi) {
        log.info("Mulai ambil data Prodi dengan kode prodi " + kodeProdi);
        ProdiModel result = prodiMapper.getProdiByKodeProdi(kodeProdi);
        if(result == null){
            log.info("Data Prodi dengan kode " + kodeProdi+ " tidak ditemukan.");
            return null;
        }
        log.info("Selesai ambil semua data Prodo. Berhasil mengambil 1 data Prodi dengan kode prodi " + result.getKodeProdi());
        log.info("Populate umur dari list peserta");
        for (PesertaModel data :
                result.getListPeserta()) {
            String tglLahir = data.getTglLahir();
            data.setUmur(pesertaServices.calculateAge(tglLahir));
        }
        log.info("Selesai populate umur dari list peserta");
        return result;
    }

    @Override
    public PesertaModel getPesertaTertua(List<PesertaModel> listPeserta) {
        return Collections.max(listPeserta);
    }

    @Override
    public PesertaModel getPesertaTermuda(List<PesertaModel> listPeserta) {
        return Collections.min(listPeserta);
    }
}
