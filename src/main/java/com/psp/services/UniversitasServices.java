package com.psp.services;

import com.psp.models.UniversitasModel;

import java.util.List;

/**
 * Created by Get-Up on 4/8/2017.
 */

public interface UniversitasServices {
    List<UniversitasModel> getAllUniversitas();

    UniversitasModel getUniversitasByKode(String kodeUniversitas);
}
