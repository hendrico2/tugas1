package com.psp.services;

import com.psp.models.PesertaModel;

/**
 * Created by Get-Up on 4/7/2017.
 */

public interface PesertaServices {
    int calculateAge(String date);
    PesertaModel getPeserta(String nomor);
}
