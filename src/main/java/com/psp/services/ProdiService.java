package com.psp.services;

import com.psp.models.PesertaModel;
import com.psp.models.ProdiModel;

import java.util.List;

/**
 * Created by Get-Up on 4/11/2017.
 */

public interface ProdiService {

    ProdiModel getProdiByKodeProdi(String kodeProdi);

    PesertaModel getPesertaTertua(List<PesertaModel> listPeserta);

    PesertaModel getPesertaTermuda(List<PesertaModel> listPeserta);
}
