package com.psp.services;

import com.psp.dao.UniversitasMapper;
import com.psp.models.UniversitasModel;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Get-Up on 4/8/2017.
 */
@Log
@Service
public class UniversitasServiceImplementation implements UniversitasServices {
    @Autowired
    UniversitasMapper universitasMapper;

    @Override
    public UniversitasModel getUniversitasByKode(String kodeUniversitas) {
        log.info("Mulai ambil data Universitas dengan kode " + kodeUniversitas);
        UniversitasModel result = universitasMapper.getUniversitasByKode(kodeUniversitas);
        if(result == null){
            log.info("Data Universitas dengan kode " + kodeUniversitas+ " tidak ditemukan.");
            return null;
        }
        log.info("Selesai ambil semua data Universitas. Berhasil mengambil 1 data universitas dengan kode " + result.getKode());
        return result;
    }

    @Override
    public List<UniversitasModel> getAllUniversitas() {
        log.info("Mulai ambil semua data Universitas");
        List<UniversitasModel> results = universitasMapper.getAllUniversitas();
        log.info("Selesai ambil semua data Universitas. Berhasil mengambil " + results.size() + "data");
        return results;
    }
}
