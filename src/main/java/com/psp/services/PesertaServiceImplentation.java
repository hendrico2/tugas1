package com.psp.services;

import com.psp.dao.PesertaMapper;
import com.psp.models.PesertaModel;
import lombok.extern.java.Log;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Get-Up on 4/7/2017.
 */
@Log
@Service
public class PesertaServiceImplentation implements PesertaServices {

    @Autowired
    PesertaMapper pesertaMapper;

    @Override
    public int calculateAge(String date) {
        String[] split = date.split("-");
        LocalDate tglLahir = new LocalDate(
                Integer.parseInt(split[0]),
                Integer.parseInt(split[1]),
                Integer.parseInt(split[2]));
        LocalDate tglSekarang = LocalDate.now();
        return Years.yearsBetween(tglLahir, tglSekarang).getYears();
    }

    @Override
    public PesertaModel getPeserta(String nomor) {
        log.info("Mulai ambil data Peserta dengan nomor " + nomor);
        PesertaModel result = pesertaMapper.getPesertaByNomor(nomor);
        if (result == null) {
            log.info("Data Peserta dengan nomor " + nomor + " tidak ditemukan.");
            return null;
        }
        log.info("Selesai ambil semua data Peserta. Berhasil mengambil 1 data peserta dengan nomor " + result.getNomor());
        return result;
    }

}
