package com.psp.controllers;

import com.psp.models.PesertaModel;
import com.psp.models.ProdiModel;
import com.psp.services.PesertaServices;
import com.psp.services.ProdiService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Get-Up on 4/8/2017.
 */

@Log
@Controller
public class PesertaController {
    @Autowired
    private PesertaServices pesertaDAO;
    @Autowired
    private ProdiService prodiDAO;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("tittle","Halaman Utama");
        model.addAttribute("peserta",new PesertaModel());
        return "index";
    }

    @PostMapping("/pengumuman/submit")
    public String viewPengumuman(Model model, @ModelAttribute PesertaModel peserta) {
        String nomor = peserta.getNomor();
        model.addAttribute("tittle","Pengumuman" );
        peserta = pesertaDAO.getPeserta(peserta.getNomor());
        if (peserta != null){
            peserta.setUmur(pesertaDAO.calculateAge(peserta.getTglLahir()));
            model.addAttribute("peserta",peserta);
            if (peserta.getKodeProdi() == null){
                model.addAttribute("note","gagal");
            } else{
                ProdiModel dataProdi = prodiDAO.getProdiByKodeProdi(peserta.getKodeProdi());
                model.addAttribute("prodi",dataProdi);
                model.addAttribute("note","lulus");
            }
            return "peserta/peserta";
        }
        model.addAttribute("nomor",nomor);
        return "peserta/peserta-not-found";
    }

    @GetMapping("/peserta")
    public String viewDetailPeserta(@RequestParam("nomor") String nomor, Model model){
        PesertaModel result = pesertaDAO.getPeserta(nomor);
        if(result == null){
            model.addAttribute("nomor",nomor);
            return "peserta/peserta-not-found";
        }
        result.setUmur(pesertaDAO.calculateAge(result.getTglLahir()));
        model.addAttribute("peserta",result);
        return "peserta/peserta-details";
    }
}
