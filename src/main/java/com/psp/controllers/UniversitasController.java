package com.psp.controllers;

import com.psp.models.UniversitasModel;
import com.psp.services.UniversitasServices;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log
@Controller
public class UniversitasController {

    @Autowired
    UniversitasServices universitasServices;

    @RequestMapping("/univ")
    public String showAllUniversitas(Model model) {
        List<UniversitasModel> listUniversitas = universitasServices.getAllUniversitas();
        model.addAttribute("listUniversitas",listUniversitas);
        model.addAttribute("title","Data Seluruh Universitas");
        return "universitas/universitas-all";
    }

    @GetMapping("/univ/{kode}")
    public String showDetailUniversitas(Model model,@PathVariable(value = "kode") String kode){
        UniversitasModel dataUniversitas = universitasServices.getUniversitasByKode(kode);
        if(dataUniversitas == null){
            return "universitas/universitas-not-found";
        }
        model.addAttribute("universitas",dataUniversitas);
        model.addAttribute("title","Data Universitas " + dataUniversitas.getNama());
        return  "universitas/universitas-details";
    }

}
