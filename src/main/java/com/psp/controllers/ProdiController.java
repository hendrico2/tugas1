package com.psp.controllers;

import com.psp.models.ProdiModel;
import com.psp.models.UniversitasModel;
import com.psp.services.ProdiService;
import com.psp.services.UniversitasServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Get-Up on 4/11/2017.
 */
@Controller
public class ProdiController {
    @Autowired
    ProdiService prodiDAO;
    @Autowired
    UniversitasServices universitasDAO;

    @RequestMapping("/prodi/{kode}")
    public String showDetailsProdi(Model model, @PathVariable("kode") String kodeProdi){
        model.addAttribute("title","Detail prodi");
        ProdiModel prodiModel = prodiDAO.getProdiByKodeProdi(kodeProdi);
        if(prodiModel == null){
            return "prodi/prodi-not-found";
        }
        UniversitasModel dataUniversitas = universitasDAO.getUniversitasByKode(prodiModel.getKodeUniversitas());
        model.addAttribute("universitas",dataUniversitas);
        model.addAttribute("prodi",prodiModel);
        model.addAttribute("pesertaTertua",prodiDAO.getPesertaTertua(prodiModel.getListPeserta()));
        model.addAttribute("pesertaTermuda",prodiDAO.getPesertaTermuda(prodiModel.getListPeserta()));
        return "prodi/prodi-details";
    }
}
